'use strict';
~ function() {
	var
	ad = document.getElementById('mainContent'),
	dcAd = {};

	window.init = function() {
        dcAd.setObjects();

	}
	
	//Function to run with any animations starting on load, or bringing in images etc
    dcAd.setObjects = function() {

        Enabler.addEventListener(studio.events.StudioEvent.ORIENTATION, dcAd.changeOrientationHandler);

        //Assign All the elements to the element on the page
        dcAd.container = document.getElementById('mainContent');
        // dcAd.bgExit = document.getElementById('background_exit_dc');

        // dcAd.bgImage1 = document.getElementById('bgImage1');
        // dcAd.bgImage2 = document.getElementById('bgImage2');

        // dcAd.cta = document.getElementById('cta_btn');

        // dcAd.addListeners();

        dcAd.changeOrientationHandler();

        // dcAd.container.style.visibility = 'visible';

        play();
    }

	//Add Event Listeners
    dcAd.addListeners = function() {
        dcAd.bgExit.addEventListener('click', dcAd.bgExitHandler, false);
        dcAd.cta.addEventListener('click', dcAd.ctaExitHandler, false);

        console.log("orientation mode: " + Enabler.getOrientation().getMode());
        console.log("orientation degrees: " + Enabler.getOrientation().getDegrees());
    }

	//exits
    dcAd.bgExitHandler = function(e) {
        dcAd.enablerExit("ON_BG_EXIT");
    }

    dcAd.ctaExitHandler = function(e) {
        dcAd.enablerExit("ON_CTA_EXIT");
	}
	
	//Function to call for Custom Exit Tracking - using switch method
    dcAd.enablerExit = function(type) {
        switch (type) {
            case "ON_CTA_EXIT":
                Enabler.exit('HTML5_CTA_Clickthrough');
                break;

            case "ON_BG_EXIT":
                Enabler.exit('HTML5_Background_Clickthrough');
                break;
        }
    }

    dcAd.changeOrientationHandler = function() {

        console.log("orientation mode: " + Enabler.getOrientation().getMode());
        console.log("orientation degrees: " + Enabler.getOrientation().getDegrees());

        if (Enabler.getOrientation().getMode() == 'portrait') {
            dcAd.container.style.width = 320 + 'px';
            dcAd.container.style.height = 480 + 'px';

            // dcAd.bgImage1.style.display = 'inline';
            // dcAd.bgImage2.style.display = 'none';
        } else {
            dcAd.container.style.width = 480 + 'px';
            dcAd.container.style.height = 320 + 'px';

            // dcAd.bgImage1.style.display = 'none';
            // dcAd.bgImage2.style.display = 'inline';
        }
    }

	function play() {

		TweenLite.to('#copyOne', 0.5,{x:0, ease: Power1.easeOut});
		TweenLite.to('#copyOne', 0.5, {delay:2,opacity:0, ease: Power1.easeOut})

		TweenLite.to('#copyTwo', 0.5, {delay:2.5, x:0, ease: Power1.easeOut})
		TweenLite.to('#copyTwo', 0.5, {delay:4.5,opacity:0, ease: Power1.easeOut})

		TweenLite.to('#copyThree', 0.5, {delay:5, x:0, ease: Power1.easeOut})
		TweenLite.to('#copyThree', 0.5, {delay:7,opacity:0, ease: Power1.easeOut})

		TweenLite.to('#copyFour', 0.5, {delay:7.5, x:0, ease: Power1.easeOut})
		TweenLite.to('#copyFour', 0.5, {delay:9.5, opacity:1, ease: Power1.easeOut})
	}


}();
